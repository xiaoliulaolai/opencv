#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

int main() {
//    cv::Mat img = cv::imread("/home/xlll/Downloads/opencv/samples/data/apple.jpg");
    cv::Mat img(2, 3, CV_64FC3, cv::Scalar(1.2, 3.4, 5.6));
    std::cout << "rows = " << img.rows << std::endl;
    std::cout << "cols = " << img.cols << std::endl;
    std::cout << "channels = " << img.channels() << std::endl;
    std::cout << "depth = " << img.depth() << std::endl;

    std::cout << "total = " << img.total() << std::endl;
    std::cout << "step1 = " << img.step1(0) << std::endl;
    std::cout << "step2 = " << img.step1(1) << std::endl;
    std::cout << "total1 = " << img.total(0, 1) << std::endl;
    std::cout << "total2 = " << img.total(1, 2) << std::endl;
    std::cout << "elemsize = " << img.elemSize() << std::endl;
    std::cout << "elemsize1 = " << img.elemSize1() << std::endl;
/*    cv::namedWindow("img", cv::WINDOW_AUTOSIZE);
    cv::imshow("img", img);

    std::cout << CV_VERSION << std::endl;
    std::cout << CV_MAJOR_VERSION << std::endl;
    std::cout << CV_MINOR_VERSION << std::endl;
    std::cout << CV_SUBMINOR_VERSION << std::endl;
    std::cout << img.flags << std::endl;
    std::cout << ((img.flags & cv::Mat::MAGIC_MASK) == cv::Mat::MAGIC_VAL) << std::endl;
    std::cout << ((img.flags & cv::Mat::TYPE_MASK) >> 3) << std::endl;
    std::cout << ((img.flags & cv::Mat::DEPTH_MASK)) << std::endl;
    std::cout << "depth = " << img.depth() << std::endl;
    std::cout << "channels = " << img.channels() << std::endl;
    std::cout << "type = " << img.type() << std::endl;
    std::cout << "continuous = " << img.isContinuous() << std::endl;
    std::cout << "submatrix = " << img.isSubmatrix() << std::endl;
    std::cout << "type2 = " << (img.flags & cv::Mat::TYPE_MASK - 8) << std::endl;
    cv::waitKey(0);
*/
    return 0;
}
