#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

int main() {
    cv::Mat image = cv::imread("/home/xlll/Downloads/opencv/samples/data/apple.jpg", cv::IMREAD_GRAYSCALE);
    if (image.empty()) {
        std::cout << "Read image failed!" << std::endl;
        return EXIT_FAILURE;
    }
    cv::namedWindow("Image", cv::WINDOW_NORMAL);
    cv::imshow("Image", image);
    cv::waitKey(0);

    return EXIT_SUCCESS;
}
