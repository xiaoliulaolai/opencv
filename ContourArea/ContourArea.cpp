#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

static double _contour_area(const std::vector<cv::Point>& contour, bool oriented) {
    int numPts = contour.size();
    if (numPts == 0) {
        return 0;
    }
    
    double area = 0;
    cv::Point xy_k = contour[numPts-1];
    for (int i = 0; i < numPts; i++) {
        cv::Point xy_k1 = contour[i];
        area += (double)xy_k.x*xy_k1.y - (double)xy_k.y*xy_k1.x;
        xy_k = xy_k1;
    }
    area *= 0.5;
    if (!oriented) {
        area = fabs(area);
    }
    return area;
}


int main(int argc, char **argv) {
    cv::Mat src = cv::imread("/home/xlll/Downloads/opencv/samples/data/detect_blob.png", cv::IMREAD_GRAYSCALE);
    if (src.empty()) {
        std::cout << "failed to read image" << std::endl;
        return EXIT_FAILURE;
    }
    
    cv::Mat binary;
    cv::Canny(src, binary, 100, 200);
    
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(binary, contours, cv::RETR_TREE, cv::CHAIN_APPROX_NONE, cv::Point());
    cv::RNG rng(100);
    cv::Mat drawing = cv::Mat::zeros(src.size(), CV_8UC3);
    bool oriented = true;
    bool allSame = true;
    for (int i = 0; i < contours.size(); i++) {
        cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
        cv::drawContours(drawing, contours, i, color, 1, 8, hierarchy);
        
        double area1 = cv::contourArea(contours[i], oriented);
        double area2 = _contour_area(contours[i], oriented);
        if (std::abs(area1-area2) > DBL_EPSILON) {
            std::cout << "two results are not the same!" << std::endl;
            std::cout << "\tOpenCV --> contour " << i << " = " << area1 << std::endl;
            std::cout << "\tNot OpenCV --> contour " << i << " = " << area2 << std::endl;
            allSame = false;
        } else {
            std::cout << "two results are the same!" << std::endl;
            std::cout << "\tarea of contour " << i << " = " << area1 << std::endl;
        }
    }
    
    std::cout << "===============================" << std::endl;
    if (!allSame) {
        std::cout << "Some results are not the same!" << std::endl;
    } else {
        std::cout << "All results are the same!" << std::endl;
    }
    
    cv::imshow("src", src);
    cv::imshow("drawing", drawing);
    cv::waitKey(0);
    return 0;
}
