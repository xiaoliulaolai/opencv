cmake_minimum_required(VERSION 3.0)
project(matchshapes)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3")
find_package(OpenCV REQUIRED)
add_executable(matchshapes MatchShapes.cpp)
target_link_libraries(matchshapes ${OpenCV_LIBS})
#install(TARGETS matchshapes RUNTIME DESTINATION bin)

